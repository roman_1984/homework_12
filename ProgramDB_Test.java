import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ProgramDB_Test {

    /**
     * Проверяю, что подключение действительно устанавливается
     */

    @Test
    void openDb_Test()
    {

        ProgramDB programDB = new ProgramDB();

        try(Connection connection = programDB.openDb())
        {
            assertTrue(connection.isValid(1));
            assertFalse(connection.isClosed());
        } catch (SQLException ex)
        {
            ex.printStackTrace();
        }
    }

    /**
     * Проверяю, успешный ввод нового User
     */
    @Test
    void insert_Test_True() {

        Person personForTest = new Person();
        personForTest.setName("Roman");
        personForTest.setPhone(7777777);

        ProgramDB dbTest = new ProgramDB();

        boolean result = dbTest.insert(personForTest,"users");
        System.out.println("Result of test: " + result);

        Assertions.assertTrue(result);
    }

    /**
     * Проверяю, не успешный ввод нового User
     */
    @Test
    void insert_Test_False() {

        Person personForTest = new Person();
        personForTest.setName("Roman1");
        personForTest.setPhone(7777777);

        ProgramDB dbTest = new ProgramDB();
     //   dbTest.open();

        boolean result = dbTest.insert(personForTest, "person");
        System.out.println("Result of test: " + result);

        Assertions.assertFalse(result);
    }

    /**
     * Проверяю, успешное удаление User по его id
     */
    @Test
    void delete() throws SQLException
    {
        ProgramDB dbTest = new ProgramDB();

        boolean result = dbTest.delete("11");
        Assertions.assertTrue(result);
    }

    /**
     * Проверяю, успешное изменение номера телефона по имени
     */
    @Test
    void change() throws SQLException {
        ProgramDB dbTest = new ProgramDB();

        String name = "Roman";
        int changePhone = 1_222_3333;

        dbTest.change(changePhone, name);

        String query = "SELECT * FROM users WHERE phone = ?";

        PreparedStatement statement = dbTest.openDb().prepareStatement(query);
        statement.setString(1, String.valueOf(changePhone));
        boolean hasResult = statement.execute();
        Assertions.assertTrue(hasResult);


    }

    /**
     * Проверяю, успешный вывод таблицы
     * 1) по первой записи в БД по имени
     * 2) дополнительная проверка по номеру телефона
     */
    @Test
    void select_Test_True() throws SQLException {

        ProgramDB dbTest = new ProgramDB();

        dbTest.select();

        String query = "SELECT * FROM users WHERE name = ?";

        PreparedStatement statement = dbTest.openDb().prepareStatement(query);
        statement.setString(1, "Evgenij");
        boolean hasResult = statement.execute();
        Assertions.assertTrue(hasResult);

        ResultSet resultSet = statement.getResultSet();
        resultSet.next();
        int phone = resultSet.getInt("phone");
        Assertions.assertEquals(1111111, phone);
    }
}