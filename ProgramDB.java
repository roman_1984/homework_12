import java.sql.*;
import java.util.Scanner;

public class ProgramDB {

    /**
     * База данных создана через программу sqlite3.exe петум запроса
     * CREATE TABLE users (
     * id INTEGER PRIMARY KEY AUTOINCREMENT,
     * name VARCHAR(50),
     * phone INTEGER(15));
     */

    public final static String URL = "jdbc:sqlite:C:\\SQLLite\\users.db";
    public final static String nameDb = "users";

    /**
     * подключение к БД
     * @return DriverManager.getConnection(URL)
     */
    public Connection openDb() throws SQLException
    {
        System.out.println("Connected to DB");

        return DriverManager.getConnection(URL);
    }


    public static void main(String[] args)
    {
        ProgramDB programDB = new ProgramDB();

//        programDB.insert(programDB.addPerson(),nameDb);
//        programDB.delete("16");
//        programDB.change(777777777,"User1");
        programDB.select();
        programDB.closeStreams();
    }

    /**
     * ввод данных в БД с клавиатуры
     * @return newPerson
     */
        public Person addPerson()
        {

            Person newPerson = new Person();

            Scanner scanner = new Scanner(System.in);
            System.out.print("Enter user name: ");
            String name = scanner.nextLine();
            newPerson.setName(name);

            System.out.print("Enter user phone: ");
            String phone = scanner.nextLine();
            newPerson.setPhone(Integer.parseInt(phone.trim()));

            return newPerson;
        }

    /**
     * создание запроса для добавления данных в БД
     * @param person
     * @return
     */
    public boolean insert(Person person, String dbName)
        {

            try (Statement statement = openDb().createStatement())
            {

                String query =
                        "INSERT INTO '" + dbName + "' (name, phone) " +
                        "VALUES ('" + person.getName() +
                        "','" + person.getPhone() + "')";


                statement.executeUpdate(query);

                System.out.println("Added");

                return true;
            }
            catch (SQLException ex)
            {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
                return false;
            }
        }

    /**
     * создание запроса для удадения данных из БД по "id"
     * @return
     */
    boolean delete(String id)
    {
        try(PreparedStatement statement = openDb().prepareStatement("DELETE FROM users WHERE id = ?"))
        {
            statement.setString(1, id);
            int resultRows = statement.executeUpdate();
            System.out.println("Delete: " + resultRows);

            if (resultRows != 0)
                return true;
            else
                return false;
        }
        catch (SQLException ex)
        {
            throw new RuntimeException("Failed to |delete| method id=" + id + " error=" + ex.toString(), ex);
        }
    }

    /**
     * создание запроса для изменения данных в БД по "name"
     */

    void change(int phone, String name)
    {
        try ( Statement statement = openDb().createStatement())
        {

            String query =
            "UPDATE users SET phone='" + phone +
            "' WHERE name='" + name + "';";

            statement.executeUpdate(query);

            System.out.println("Change");
        }
        catch (SQLException ex)
        {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     * создание запроса для вывода всей БД
     */
        public void select()
        {
            try(Statement statement = openDb().createStatement())
            {
                String query = "SELECT id, name, phone FROM users";

                try(ResultSet resultSet = statement.executeQuery (query))
                {
                    while (resultSet.next())
                    {
                        int id = resultSet.getInt("id");
                        String name = resultSet.getString("name");
                        int phone = resultSet.getInt("phone");
                        System.out.println(id + "\t| " + name + "\t| " + phone);
                    }
                }
            }
            catch (SQLException ex)
            {
                throw new RuntimeException("Failed to |select| method error=" + ex.toString(), ex);
            }
        }

    /**
     * закрываем поток Connection
     */
        public void closeStreams()
        {

            try
            {
                openDb().close();
                System.out.println("Connections - close");;
            }
            catch (SQLException ex)
            {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }


