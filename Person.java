/**
 * сущность Person для работы с БД
 * включает в себя поля
 * name - имя
 * phone - телефон
 */
public class Person {

    private String name;
    private int phone;

    public  String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getPhone()
    {
        return phone;
    }

    public void setPhone(int phone)
    {
        this.phone = phone;
    }

   @Override
   public String toString(){
        return "Person{" +
                "name=" + name + '\'' +
                ", phone"  + phone +
                '}';
    }

}
